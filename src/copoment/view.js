import React, { Component } from 'react'
import Axios from 'axios';
export default class View extends Component {
    constructor() {
        super();
        this.state = {
            post: [],
        };
    }
    componentWillMount() {
        var id = this.props.match.params.id
        Axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`).then((res) => {
            this.setState({
               post: res.data.DATA,
            });
        })
    }
    render() {
        return (
            <div>
                
            </div>
        )
    }
}
